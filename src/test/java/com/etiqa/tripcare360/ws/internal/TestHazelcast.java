package com.etiqa.tripcare360.ws.internal;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.etiqa.tripcare360.ws.internal.constant.HazelcastConstant;
import com.etiqa.tripcare360.ws.internal.service.HazelcastService;
import com.hazelcast.core.IQueue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestHazelcast {

	@Autowired
	private HazelcastService hazelcastService;

	@Test
	public void testMapPutString() throws Exception {
		String value = "test.map.value";
		String key = "test.map.string.key";
		hazelcastService.put(key, value);
		assertEquals("Successful put should return: " + value, value, value);
	}
	
	@Test
	public void testMapPutObject() throws Exception {
		Integer value = 1;
		String key = "test.map.object.key";
		hazelcastService.put(key, value);
		assertEquals("Successful put should return: " + value, value, value);
	}
	
	@Test
	public void testQueueAdd() throws Exception {
		Integer value = 1;
		hazelcastService.addQueue(value);
		assertEquals("Successful add should return: " + value, value, value);
	}
	
	@Test
	public void testMapContainsKey() throws Exception {
		String key = "test.map.key";
		assertEquals("Successful contains should return: " + false, false, hazelcastService.containsKey(key));
	}
	
	@Test
	public void testMapGetValueString() throws Exception {
		String key = "test.map.string.key";
		String value = "test.map.value";
		hazelcastService.putNonAsync(key, value);
		
		String result = (String) hazelcastService.get(key);
		assertEquals("Successful get should return: " + value, value, result);
	}
	
	@Test
	public void testMapGetValueObject() throws Exception {
		String key = "test.map.object.key";
		Integer value = 1;
		hazelcastService.putNonAsync(key, value);
		
		Integer result = (Integer) hazelcastService.get(key);
		assertEquals("Successful get should return: " + value, value, result);
	}
	
	@Test
	public void testQueueGet() throws Exception {
		IQueue<Object> queue = hazelcastService.getQueue(HazelcastConstant.ERROR_QUEUE_KEY);
		Boolean result = queue != null;
		assertEquals("Successful get should return: " + true, true, result);
	}

	@Test
	public void testMapDelete() throws Exception {
		Integer value = 1;
		hazelcastService.delete("test.map.key");
		assertEquals("Successful delete should return: " + value, value, value);
	}
	
	@Test
	public void testMapClearAllCache() throws Exception {
		Integer value = 1;
		hazelcastService.clearAllCache();
		assertEquals("Successful clear should return: " + value, value, value);
	}
	
}
