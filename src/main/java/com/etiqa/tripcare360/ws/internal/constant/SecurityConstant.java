package com.etiqa.tripcare360.ws.internal.constant;

public final class SecurityConstant {

	private SecurityConstant() {
		throw new IllegalStateException("Utility class");
	}

	public static final String AUTH_LOGIN_URL = "/api/authenticate";

	// Signing key for HS512 algorithm
	// You can use the page http://www.allkeysgenerator.com/ to generate all
	// kinds of keys
	public static final String JWT_SECRET = "n2r5u8x/A%D*G-KaPdSgVkYp3s6v9y$B&E(H+MbQeThWmZq4t7w!z%C*F-J@NcRf";

	public static final String TOKEN_HEADER = "Authorization";

	public static final String TOKEN_PREFIX = "Bearer ";

	public static final String TOKEN_TYPE = "JWT";

	public static final String TOKEN_ISSUER = "secure-api";

	public static final String TOKEN_AUDIENCE = "secure-app";

	public static final Integer BCRYPT_PASSWORD_ENCODER_CONSTANT = 5;
}
