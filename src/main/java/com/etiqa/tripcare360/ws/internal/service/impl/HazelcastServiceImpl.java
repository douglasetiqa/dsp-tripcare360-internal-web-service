package com.etiqa.tripcare360.ws.internal.service.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.etiqa.tripcare360.ws.internal.constant.HazelcastConstant;
import com.etiqa.tripcare360.ws.internal.service.HazelcastService;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;

@Service
public class HazelcastServiceImpl implements HazelcastService {

	private static final Logger logger = LoggerFactory.getLogger(HazelcastServiceImpl.class);

	private final HazelcastInstance hazelcastInstance;

	@Autowired
	HazelcastServiceImpl(HazelcastInstance hazelcastInstance) {
		this.hazelcastInstance = hazelcastInstance;
	}

	@Override
	@Async("threadPoolTaskExecutor")
	public void put(String key, String value) {
		Map<String, Object> hazelcastMap = new HashMap<>();
		try {
			hazelcastMap = hazelcastInstance.getMap(HazelcastConstant.DSP_MAP_KEY);
			hazelcastMap.put(key, value);
		} catch (Exception ex) {
			hazelcastMap.clear();
			addQueue(ex);
			logger.error(ex.getMessage(), ex);
		}
	}

	@Override
	@Async("threadPoolTaskExecutor")
	public void put(String key, Object value) {
		Map<String, Object> hazelcastMap = new HashMap<>();
		try {
			hazelcastMap = hazelcastInstance.getMap(HazelcastConstant.DSP_MAP_KEY);
			hazelcastMap.put(key, value);
		} catch (Exception ex) {
			addQueue(ex);
			logger.error(ex.getMessage(), ex);
			hazelcastMap.clear();
		}
	}
	
	@Override
	public void putNonAsync(String key, Object value) {
		Map<String, Object> hazelcastMap = new HashMap<>();
		try {
			hazelcastMap = hazelcastInstance.getMap(HazelcastConstant.DSP_MAP_KEY);
			hazelcastMap.put(key, value);
		} catch (Exception ex) {
			addQueue(ex);
			logger.error(ex.getMessage(), ex);
			hazelcastMap.clear();
		}
	}
	
	@Override
	public void putNonAsync(String key, String value) {
		Map<String, Object> hazelcastMap = new HashMap<>();
		try {
			hazelcastMap = hazelcastInstance.getMap(HazelcastConstant.DSP_MAP_KEY);
			hazelcastMap.put(key, value);
		} catch (Exception ex) {
			addQueue(ex);
			logger.error(ex.getMessage(), ex);
			hazelcastMap.clear();
		}
	}

	@Override
	@Async("threadPoolTaskExecutor")
	public void addQueue(Object value) {
		Queue<Object> queue = new LinkedList<>();
		try {
			queue = hazelcastInstance.getQueue(HazelcastConstant.ERROR_QUEUE_KEY);
			Map<String, Object> map = new HashMap<>();
			map.put(HazelcastConstant.ERROR_TIMESTAMP_KEY, new DateTime().toString());
			map.put(HazelcastConstant.ERROR_DATA_KEY, value);
			queue.add(map);
		} catch (Exception ex) {
			addQueue(ex);
			logger.error(ex.getMessage(), ex);
			queue.clear();
		}
	}

	@Override
	public boolean containsKey(String key) {
		try {
			Map<String, Object> hazelcastMap = hazelcastInstance.getMap(HazelcastConstant.DSP_MAP_KEY);
			return hazelcastMap.containsKey(key);
		} catch (Exception ex) {
			addQueue(ex);
			logger.error(ex.getMessage(), ex);
			return false;
		}
	}

	@Override
	public Object get(String key) {
		Map<String, Object> hazelcastMap = null;
		try {
			hazelcastMap = hazelcastInstance.getMap(HazelcastConstant.DSP_MAP_KEY);
			return hazelcastMap.get(key);
		} catch (Exception ex) {
			addQueue(ex);
			logger.error(ex.getMessage(), ex);
			return null;
		}
	}

	@Override
	public IQueue<Object> getQueue(String key) {
		try {
			return hazelcastInstance.getQueue(HazelcastConstant.ERROR_QUEUE_KEY);
		} catch (Exception ex) {
			addQueue(ex);
			return null;
		}
	}

	@Override
	@Async("threadPoolTaskExecutor")
	public void delete(String key) {
		Map<String, Object> hazelcastMap = hazelcastInstance.getMap(HazelcastConstant.DSP_MAP_KEY);
		hazelcastMap.remove(key);
	}

	@Override
	@Async("threadPoolTaskExecutor")
	public void clearAllCache() {
		Map<String, Object> hazelcastMap = hazelcastInstance.getMap(HazelcastConstant.DSP_MAP_KEY);
		hazelcastMap.clear();
	}
}
