package com.etiqa.tripcare360.ws.internal.constant;

public final class ExceptionConstant {

	private ExceptionConstant() {
		throw new IllegalStateException("Utility class");
	}

	public static final String NRIC_PATTERN = "Variable nric must be in 12 digits";
	
	public static final String HEIGHT_PATTERN = "Variable height must be in digits";
	
	public static final String WEIGHT_PATTERN = "Variable weight must be in digits";

	public static final String QQID_PATTERN = "Variable qqid must be in digits";

	public static final String KEY_BLANK = "Key should not be blank";

	public static final String PAYMENT_MODE_PATTERN = "Variable paymentMode must be either 1(Annually) or 12(Monthly)";

	public static final String SUM_COVERED_MIN_EZY_SECURE = "Variable sumCovered should not be less than 100000";

	public static final String SUM_COVERED_MAX_EZY_SECURE = "Variable sumCovered should not be greater than 500000";

	public static final String SUM_COVERED_NULL = "Variable sumCovered should not be null";

	public static final String DURATION_OF_BENEFIT_MIN = "Variable durationOfBenefit should not be less than 5";

	public static final String DURATION_OF_BENEFIT_NULL = "Variable durationOfBenefit should not be null";

	public static final String GENDER_BLANK = "Variable gender should not be blank";

	public static final String GENDER_PATTERN = "Variable gender must be either m (Male) or f (Female)";

	public static final String SMOKER_BLANK = "Variable isSmoker should not be blank";

	public static final String SMOKER_PATTERN = "Variable isSmoker must be either y (Yes) or n (No)";
	
	public static final String UNDERWRITING_PATTERN = "Answer must be either y (Yes) or n (No)";

	public static final String DATE_OF_BIRTH_BLANK = "Variable dateOfBirth cannot be blank";

	public static final String DATE_OF_BIRTH_PATTERN = "Variable dateOfBirth must be in form of DD/MM/YYYY";
	
	public static final String NAME_BLANK = "Name cannot be blank";
	
	public static final String RACE_BLANK = "Race code should not be blank";
	
	public static final String RELIGION_BLANK = "Religion code should not be blank";
	
	public static final String NATIONALITY_BLANK = "Nationality code should not be blank";
	
	public static final String SALARY_BLANK = "Salary code should not be blank";
	
	public static final String OCCUPATION_BLANK = "Occupation code should not be blank";
	
	public static final String INDUSTRY_BLANK = "Industry code should not be blank";
	
	public static final String EMPLOYER_BLANK = "Employer name should not be blank";
	
	public static final String ADDRESS1_BLANK = "Address1 should not be blank";
	
	public static final String ADDRESS2_BLANK = "Address2 should not be blank";

	public static final String POSTCODE_BLANK = "Postcode should not be blank";
	
	public static final String STATE_BLANK = "State should not be blank";
	
	public static final String RACE_PATTERN = "Race code is not valid";
	
	public static final String RELIGION_PATTERN = "Religion code is not valid";

	public static final String NATIONALITY_PATTERN = "Nationality code is not valid";
	
	public static final String SALARY_PATTERN = "Salary code is not valid";
}
