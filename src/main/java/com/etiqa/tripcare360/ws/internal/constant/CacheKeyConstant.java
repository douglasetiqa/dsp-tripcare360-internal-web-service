package com.etiqa.tripcare360.ws.internal.constant;

public final class CacheKeyConstant {

	private CacheKeyConstant() {
		throw new IllegalStateException("Utility class");
	}

	public static final String BRUTE_FORCE_IP_CACHE_KEY = "tripcare360.brute.force.ip.%s";

	public static final String PRODUCT_NAME_ATTRIBUTE_CACHE_KEY = "tripcare360.attribute.cache.%s";

	public static final String PRODUCT_NAME_OCCUPATION_CACHE_KEY = "tripcare360.occupation.cache.%s";

	public static final String PRODUCT_NAME_PREMIUM_CACHE_KEY = "tripcare360.premium.cache.%s.%s.%s.%s.%s";

	public static final String PRODUCT_NAME_PREMIUM_DETAILS_CACHE_KEY = "tripcare360.premium.details.cache.%s.%s.%s.%s.%s";

	public static final String PRODUCT_NAME_TSAR_CACHE_KEY = "tripcare360.tsar.cache.%s.%s.%s.%s.%s.%s.%s.%s";

	public static final String PRODUCT_NAME_CLAIM_HISTORY_CACHE_KEY = "tripcare360.claim.history.cache.%s";

	public static final String PRODUCT_NAME_LIAM_CACHE_KEY = "tripcare360.liam.cache.%s";

	public static final String PRODUCT_NAME_TERRORIST_CACHE_KEY = "tripcare360.terrorist.cache.%s";

	public static final String PRODUCT_NAME_REJECTION_HISTORY_CACHE_KEY = "tripcare360.rejection.history.cache.%s";
	
	public static final String PRODUCT_NAME_QQ_CACHE_KEY = "tripcare360.qq.cache.%s";

	public static final String PRODUCT_NAME_CUSTOMER_DETAILS_CACHE_KEY = "tripcare360.customer.details.cache.%s";

	public static final String PRODUCT_NAME_VPMS_CACHE_KEY = "tripcare360.vpms.cache.%s";
}
