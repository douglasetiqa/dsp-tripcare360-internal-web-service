package com.etiqa.tripcare360.ws.internal.constant;

import java.util.Arrays;
import java.util.List;

public final class CommonConstant {

	private CommonConstant() {
		throw new IllegalStateException("Utility class");
	}

	public static final String PRODUCT_CODE_MAIN = "TL";

	public static final String PRODUCT_CODE_CLS = "PCTB01";

	public static final String AGENT_ACTIVE_CODE = "1";

	public static final String VPMS_DOB_FORMAT = "dd.MM.yyyy";

	public static final String VPMS_RESTFUL_FORMAT = "dd/MM/yyyy";

	public static final String ORACLE_DOB_FORMAT = "dd-MMM-yyyy";

	public static final String NRIC_DOB_FORMAT = "yyMMdd";

	public static final Integer EZYSECURE_MAX_AGE_PROTECTION = 80;

	public static final String CLAIM_HISTORY_COUNT_KEY = "claimCount";

	public static final String MALE_GENDER_TEXT = "M";

	public static final String FEMALE_GENDER_TEXT = "F";

	public static final String ID_TYPE_CODE_NRIC = "001";

	public static final String MALE_GENDER_CODE = "001";

	public static final String FEMALE_GENDER_CODE = "002";

	public static final String DATA_TEXT = "data";

	public static final String STATUS_MESSAGE_TEXT = "message";

	public static final String TSAR_AVAILABLE_TEXT = "available";

	public static final String TSAR_PURCHASED_TEXT = "purchased";

	public static final String PREMIUM_FREQUENCY_TYPE_TEXT_MONTHLY = "monthly";

	public static final String PREMIUM_FREQUENCY_TYPE_TEXT_ANNUALLY = "annually";

	public static final String PREMIUM_FREQUENCY_TYPE_CODE_MONTHLY = "12";

	public static final String PREMIUM_FREQUENCY_TYPE_CODE_ANNUALLY = "1";

	public static final String LANGUAGE_CODE_ENGLISH = "E";

	public static final String API_INDICATOR = "API";

	public static final String QUOTATION_STATUS_OPEN = "O";

	public static final String QUOTATION_STATUS_COMPLETE = "C";

	public static final String QUOTATION_STATUS_REJECTED = "R";

	public static final String REJECTION_TEXT_LIAM = "LIAM List";

	public static final String REJECTION_TEXT_CLAIM_HISTORY = "Claim History";

	public static final String REJECTION_TEXT_TERRORIST = "Terrorist List";

	public static final String REJECTION_TEXT_TSAR = "TSAR";

	public static final String REJECTION_TEXT_BMI = "BMI";

	public static final String REJECTION_TEXT_UNDERWRITING_1 = "HQ1";

	public static final String REJECTION_TEXT_UNDERWRITING_2 = "HQ2";

	public static final String REJECTION_TEXT_UNDERWRITING_3 = "HQ3";

	public static final String REJECTION_TEXT_REJECTED_BEFORE = "Rejected Before";

	public static final String REJECTION_TEXT_REJECTED_NATIONALITY = "Nationality";

	public static final Integer DROP_IN_QQ_DEFAULT = 2;

	public static final String EMPTY_STRING = "";

	public static final String PARAM_KEY_UNDERWRITING = "uw";

	public static final String PARAM_DESCRIPTION_UNDERWRITING = "Underwriting Question ";

	public static final String PARAM_KEY_INCOME = "income";

	public static final String ORACLE_PARAM_KEY_INCOME = "monthly income";

	public static final List<String> PARAM_KEY_LIST = Arrays.asList("religion", "gender", "race", "nationality");

	public static final String RESPONSE_TIME_STRING_FORMAT = "%,.2f ms";

	public static final Double NANO_TO_MILLIS = 1000000.0;

	public static final int AGE_NEXT_BIRTHDAY_MINIMUM = 20;

	public static final int AGE_NEXT_BIRTHDAY_MAXIMUM = 45;

	public static final String COUNTRY_CODE_MALAYSIA = "mal";

	public static final int MAIL_EXIST_COUNTER_NUMBER = 4;

	public static final String BMI_KEY = "BMI";

	public static final String BMI_DOUBLE_FORMAT = "#0.00";

	public static final Double PERCENTAGE_ONE_HUNDRED = 100.0;

	public static final String UNDERWRITING_REGEX = "y|n";

	public static final String UNDERWRITING_ANSWER_Y = "y";

	public static final String UNDERWRITING_ANSWER_N = "n";

	public static final String UNDERWRITING_KEY_PREFIX = "VALIDATION_FAILED_UNDERWRITING_";

	public static final int NRIC_LENGTH = 12;

	public static final int NRIC_DOB_LENGTH = 6;
}
