package com.etiqa.tripcare360.ws.internal.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.etiqa.tripcare360.ws.internal.constant.HazelcastConstant;
import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;
import com.hazelcast.config.QueueConfig;

@Configuration
public class HazelcastConfiguration {

	@Bean
	public Config hazelCastConfig() {

		Config config = new Config();
		config.setInstanceName("hazelcast-instance")
				.addMapConfig(
						new MapConfig().setName("map-configuration")
								.setTimeToLiveSeconds(HazelcastConstant.TIME_TO_LIVE_SECONDS)
								.setMaxSizeConfig(new MaxSizeConfig(HazelcastConstant.MAXIMUM_SIZE_CONFIG,
										MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE))
								.setEvictionPolicy(EvictionPolicy.LRU))
				.addQueueConfig(
						new QueueConfig().setName("queue-configuration")
								.setMaxSize(HazelcastConstant.QUEUE_MAXIMUM_SIZE));
		return config;
	}
}
