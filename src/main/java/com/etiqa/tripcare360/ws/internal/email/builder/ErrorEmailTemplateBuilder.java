package com.etiqa.tripcare360.ws.internal.email.builder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Component
public class ErrorEmailTemplateBuilder {

	private TemplateEngine templateEngine;

	@Autowired
	public ErrorEmailTemplateBuilder(TemplateEngine templateEngine) {
		this.templateEngine = templateEngine;
	}

	public String build(String errorMsg) {
		Context ctx = new Context(LocaleContextHolder.getLocale());
		ctx.setVariable("errorMsg", errorMsg);
		return templateEngine.process("email/error", ctx);
	}

}
