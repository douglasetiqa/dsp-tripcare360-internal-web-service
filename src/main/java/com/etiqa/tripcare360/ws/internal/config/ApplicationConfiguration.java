package com.etiqa.tripcare360.ws.internal.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.context.request.RequestContextListener;

import com.etiqa.tripcare360.ws.internal.constant.SecurityConstant;

@Configuration
@MapperScan("com.etiqa.tripcare360.ws.internal.mapper")
public class ApplicationConfiguration {

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(SecurityConstant.BCRYPT_PASSWORD_ENCODER_CONSTANT);
	}

	@Bean
	public RequestContextListener requestContextListener() {
		return new RequestContextListener();
	}

	@Bean
	@Autowired
	public LocalValidatorFactoryBean getValidator(MessageSource msgSource) {
		LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
		bean.setValidationMessageSource(msgSource);
		return bean;
	}

}
