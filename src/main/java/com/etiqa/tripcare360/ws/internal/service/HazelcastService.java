package com.etiqa.tripcare360.ws.internal.service;

import com.hazelcast.core.IQueue;

public interface HazelcastService {

	public void put(String key, String value);

	public void put(String key, Object value);
	
	public void putNonAsync(String key, Object value);
	
	public void putNonAsync(String key, String value);

	public Object get(String key);

	public void delete(String key);

	public void clearAllCache();

	public boolean containsKey(String key);

	public IQueue<Object> getQueue(String key);

	public void addQueue(Object value);

}
