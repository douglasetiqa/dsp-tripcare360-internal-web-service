package com.etiqa.tripcare360.ws.internal.constant;

public class HazelcastConstant {

	private HazelcastConstant() {
		throw new IllegalStateException("Utility class");
	}

	public static final String ERROR_QUEUE_KEY = "dsp.error.queue";

	public static final String DSP_MAP_KEY = "dsp.map";

	public static final String ERROR_TIMESTAMP_KEY = "timestamp";

	public static final String ERROR_DATA_KEY = "data";

	public static final int TIME_TO_LIVE_SECONDS = 86400;

	public static final int MAXIMUM_SIZE_CONFIG = 200;

	public static final int QUEUE_MAXIMUM_SIZE = 0;

}
