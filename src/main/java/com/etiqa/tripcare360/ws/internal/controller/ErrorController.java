package com.etiqa.tripcare360.ws.internal.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorController {

	@GetMapping("/en/error")
	public String error(Model model) {
		return "error";
	}
	
}
