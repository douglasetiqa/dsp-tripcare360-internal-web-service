package com.etiqa.tripcare360.ws.internal.email;

import java.io.File;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
class SpringMailClient implements MailClient {

	private Logger logger = LoggerFactory.getLogger(SpringMailClient.class);
	private JavaMailSender mailSender;

	@Autowired
	public SpringMailClient(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	@Override
	public void sendEmail(List<String> receivers, String subject, String content, List<String> attachments) {
		logger.info("sendEmail -> start");
		try {
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setTo(receivers.toArray(new String[receivers.size()]));
			helper.setSubject(subject);
			helper.setText(content, true);
			if (attachments != null) {
				for (String a : attachments) {
					FileSystemResource file = new FileSystemResource(new File(a));
					helper.addAttachment(file.getFilename(), file);
				}
			}
			mailSender.send(message);
			logger.info("sendEmail -> done");
		} catch (MessagingException e) {
			logger.error(e.getMessage(), e);
			logger.info("sendEmail -> failed");
		}
	}

}
