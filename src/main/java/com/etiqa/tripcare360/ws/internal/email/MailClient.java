package com.etiqa.tripcare360.ws.internal.email;

import java.util.List;

/**
 * Abstraction of mail client implementation.
 * 
 * @author Chun Hoong
 *
 */
public interface MailClient {

	public void sendEmail(List<String> receivers, String subject, String content, List<String> attachments);

}
