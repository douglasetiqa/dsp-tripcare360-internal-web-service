package com.etiqa.tripcare360.ws.internal.util;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.soap.SOAPMessage;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.etiqa.tripcare360.ws.internal.constant.CommonConstant;

public class CommonUtils {

	private static final Logger logger = LoggerFactory.getLogger(CommonUtils.class);

	public static Integer verifyIcWithBirthDate(String dateOfBirth, String dateFormat, String strNric) {
		Integer isVerified = 0;
		try {
			if (StringUtils.trimToNull(dateOfBirth) != null && StringUtils.trimToNull(dateFormat) != null
					&& StringUtils.trimToNull(strNric) != null) {
				strNric = StringUtils.substring(strNric, 0, 6);
				DateTimeFormatter formatterDob = DateTimeFormat.forPattern(dateFormat);
				DateTimeFormatter formatterIc = DateTimeFormat.forPattern(CommonConstant.NRIC_DOB_FORMAT);
				DateTime dtDateOfBirth = formatterDob.parseDateTime(dateOfBirth);
				DateTime dtNric = formatterIc.parseDateTime(strNric);
				if (dtDateOfBirth.getMillis() == dtNric.getMillis()) {
					isVerified = 1;
				}
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return isVerified;
	}

	public static String formatDobWithFormat(String dateOfBirth, String dateFormat, String outputDateFormat) {
		DateTimeFormatter formatterSiDate = DateTimeFormat.forPattern(outputDateFormat);
		DateTime dtDateOfBirth = new DateTime();
		try {
			if (StringUtils.trimToNull(dateOfBirth) != null && StringUtils.trimToNull(dateFormat) != null) {
				DateTimeFormatter formatter = DateTimeFormat.forPattern(dateFormat);
				dtDateOfBirth = formatter.parseDateTime(dateOfBirth);
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return formatterSiDate.print(dtDateOfBirth);
	}

	public static Integer getAge(String dateOfBirth, String dateFormat) {
		Integer age = -1;
		try {
			if (StringUtils.trimToNull(dateOfBirth) != null && StringUtils.trimToNull(dateFormat) != null) {
				DateTimeFormatter formatter = DateTimeFormat.forPattern(dateFormat);
				DateTime dtDateOfBirth = formatter.parseDateTime(dateOfBirth);
				DateTime dtCurrent = new DateTime();
				Years yearsAge = Years.yearsBetween(dtDateOfBirth, dtCurrent);
				age = yearsAge.getYears();
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return age;
	}

	public static String getGenderCode(String gender) {
		String genderCode = null;
		try {
			if (StringUtils.trimToNull(gender) != null) {
				if (gender.equals(CommonConstant.MALE_GENDER_TEXT)) {
					genderCode = CommonConstant.MALE_GENDER_CODE;
				} else if (gender.equals(CommonConstant.FEMALE_GENDER_TEXT)) {
					genderCode = CommonConstant.FEMALE_GENDER_CODE;
				}
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return genderCode;
	}

	public static void writeToBaos(SOAPMessage request) {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			request.writeTo(baos);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
	}

	public static String formatOracleDate() {
		SimpleDateFormat sdf = new SimpleDateFormat(CommonConstant.ORACLE_DOB_FORMAT);
		return sdf.format(new Date());
	}

	public static String formatVpmsDate() {
		SimpleDateFormat sdf = new SimpleDateFormat(CommonConstant.VPMS_DOB_FORMAT);
		return sdf.format(new Date());
	}
}
