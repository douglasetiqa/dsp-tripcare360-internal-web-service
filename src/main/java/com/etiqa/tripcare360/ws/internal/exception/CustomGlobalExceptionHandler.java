package com.etiqa.tripcare360.ws.internal.exception;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {

	private static final String TIMESTAMP_KEY = "timestamp";
	private static final String STATUS_KEY = "status";
	private static final String ERRORS_KEY = "errors";

	/**
	 * To handle error message thrown from query parameter bean validation at class
	 * level.
	 */
	@Override
	protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status,
			WebRequest request) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put(TIMESTAMP_KEY, new Date());
		body.put(STATUS_KEY, status.value());
		List<String> errors = ex.getBindingResult().getFieldErrors().stream().map(FieldError::getDefaultMessage)
				.collect(Collectors.toList());
		body.put(ERRORS_KEY, errors);
		return new ResponseEntity<>(body, headers, status);
	}

	/**
	 * To handle error message thrown from request body bean validation.
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put(TIMESTAMP_KEY, new Date());
		body.put(STATUS_KEY, status.value());
		List<String> errors = ex.getBindingResult().getAllErrors().stream().map(ObjectError::getDefaultMessage)
				.collect(Collectors.toList());
		body.put(ERRORS_KEY, errors);
		return new ResponseEntity<>(body, headers, status);
	}

	/**
	 * To handle error message thrown from query parameter bean validation at method
	 * argument level.
	 */

	@ExceptionHandler({ ConstraintViolationException.class })
	public ResponseEntity<Object> handleConstraintViolation(final ConstraintViolationException ex,
			final WebRequest request) {
		Map<String, Object> body = new LinkedHashMap<>();
		body.put(TIMESTAMP_KEY, new Date());
		body.put(STATUS_KEY, HttpStatus.BAD_REQUEST.value());
		List<String> errors = ex.getConstraintViolations().stream().map(ConstraintViolation::getMessage)
				.collect(Collectors.toList());
		body.put(ERRORS_KEY, errors);
		return new ResponseEntity<>(body, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

}
