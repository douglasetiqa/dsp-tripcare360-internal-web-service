# dsp-tripcare360-internal-web-service

This is the REST web service made to be consumed by the DSP TripCare web application.

Do not confuse this over **DSP-TRIPCARE360-API**, which is made to be consume by external party.